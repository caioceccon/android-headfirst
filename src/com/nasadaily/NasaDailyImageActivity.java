package com.nasadaily;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nasadaily.nasadailyimage.R;

public class NasaDailyImageActivity extends Activity {
  ProgressDialog dialog;
	private IotdHandler iotdHandler;

	Handler handler;
	Bitmap image;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    handler = new Handler();
    refreshFromFeed();
  }

  public void onRefresh(View view) {
    refreshFromFeed();
  }

  private void refreshFromFeed() {
    dialog = ProgressDialog.show(this, "Loading",
                                 "Loading the image of the Day");
		iotdHandler = new IotdHandler();
    Thread th = new Thread() {
      public void run() {
    	  iotdHandler.processFeed();
    		image = iotdHandler.getUrl();
        handler.post(new Runnable () {
    		  public void run() {
    			  iotdHandler.processFeed();
    				resetDisplay(iotdHandler.getTitle(), iotdHandler.getDate(),
    			               iotdHandler.getUrl(), iotdHandler.getDescription());
    				dialog.dismiss();
    			}
        });
    	}
    };
    th.start();
  }

  public void onSetWallpaper(View view) {
   	Thread th = new Thread() {
      public void run() {
    	  WallpaperManager wallpaperManager =
    		  WallpaperManager.getInstance(NasaDailyImageActivity.this);
    		try {
    		  wallpaperManager.setBitmap(image);
          handler.post(
            new Runnable() {
              public void run() {
                Toast.makeText(NasaDailyImageActivity.this, "Wallpaper set",
                               Toast.LENGTH_SHORT).show();
              }
            }
          );
    		} catch (Exception e) {
    		  e.printStackTrace();
              handler.post(
                new Runnable() {
                  public void run() {
                    Toast.makeText(NasaDailyImageActivity.this,
                                   "Error: Can`t set wallpaper!",
                                   Toast.LENGTH_SHORT).show();
                  }
                }
              );
    		}
    	}
      };
    th.start();
  }

  private void resetDisplay(String title, String date,
                            Bitmap bitmap, StringBuffer stringBuffer) {
    TextView titleView = (TextView)findViewById(R.id.imageTitle);
    titleView.setText(title);
    TextView dateView = (TextView)findViewById(R.id.imageDate);
    dateView.setText(date);
    ImageView imageView = (ImageView)findViewById(R.id.imageDisplay);
    imageView.setImageBitmap(bitmap);
    TextView descriptionView = (TextView)findViewById(R.id.imageDescription);
    descriptionView.setText(stringBuffer);
  }
}
